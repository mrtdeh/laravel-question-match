<?php

use Illuminate\Database\Seeder;

use App\Answer;
use App\Question;
use App\Match;

class MatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Match::class, 3)->create()
        ->each(function ($match) {

            factory(Question::class,10)->create([
                "match_id" => $match->id,

            ])->each(function ($question) {
                
                $answers = factory(Answer::class,4)->create([
                    "question_id" => $question->id,
                    "correct" => false,
                ]);
                
                $question->answers[1]->correct = true;
                $question->answers[1]->save();
            });

        });
    }
}
