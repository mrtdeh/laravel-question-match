<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(App\User::class, 10)->create();

        $users->each(function ($user) {

            $user->posts()->save(factory(App\Post::class)->make());

        });
    }
}
