<?php

use Illuminate\Database\Seeder;

// use Role, Permission;


class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'writer‍‍']);
        Role::create(['name' => 'editor‍‍']);
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'super']);

        Permission::create(['name' => 'delete articles']);
        Permission::create(['name' => 'edit articles']);
        Permission::create(['name' => 'manage users']);
        
    }
}
