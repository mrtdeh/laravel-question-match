<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;


use Carbon\Carbon;

class Post extends Model
{
    

    protected $fillable = [

        'user_id',
        'title',
        'content',

    ];
    

    protected $append = [
        'created_date'
    ];


    public function getCreatedDateAttribute()
    {
        return Carbon::parse($this->created_at)->toString();
    }
        
    

    public function user()
    {

        return $this->belongsTo(User::class);
        
    }

}

