<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Answer;
use App\Match;

class Question extends Model
{
    
    protected $fillable = [

        "text", 
        "answer_1", 
        "answer_2",
        "answer_3",
        "answer_4",
    ];


    protected $hidden = [

        "correct_answer"
    ];


    public function match()
    {
        return $this->belongsTo(Match::class);
    }
        

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
        
}
