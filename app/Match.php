<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Question;

class Match extends Model
{
    

    protected $fillable = [

        "description",
        "startDate",
        "endDate"
    ];


    public function questions()
    {
        return $this->hasMany(Question::class);
    }
        
}
