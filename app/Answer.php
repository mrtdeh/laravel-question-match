<?php

namespace App;

use App\Question;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    

    protected $fillable =[
        "text" 
    ];


    protected $hidden = [
        "correct"
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
        

}
