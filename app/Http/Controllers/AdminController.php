<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User, App\Post;
use Auth, Permission, Role;

class AdminController extends Controller
{



    public function __construct()
    {
        $this->middleware("auth");

    }
        
    

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    
    public function users()
    {
        $users =  User::OrderBy("id","asc")->get();
        return view("admin.users.index",[

            "users" => $users,
            
        ]);
    }


    public function posts()
    {
        $posts = Post::OrderBy("id","desc")->get();
        return view('admin.posts.index', compact("posts"));
    }


    public function permissions()
    {
        $permissions = Permission::getPermissions();
        return view("admin.users.permissions",["permissions" => $permissions]);  
    }
        
        
        

}
