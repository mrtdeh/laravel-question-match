<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class ShowUserPosts extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($username)
    {
        
        $user = User::hasUsername($username);

        return view("userPosts",["selectedUser" => $user]);
    }
}
