<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

use App\Post;

use Carbon\Carbon;


class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $append = [
        'username', 'registered_date'
    ];


    public function posts()
    {
        return $this->hasMany(Post::class);
    }


    public function getRegisteredDateAttribute()
    {
        return  $this->created_at->diffForHumans();
    }
        
    

    public function getUsernameAttribute()
    {
       
        return explode("@", $this->email)[0];
    }


    public function scopeHasUsername($query, $username)
    {
        return $query->where("email","like",$username.'@%')->first();
    }
        


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }


        
}
