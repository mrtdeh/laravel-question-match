<?php








Route::get('dashboard', 'AdminController@dashboard')->name("dashboard");

Route::get('users','AdminController@users')->name("users");

Route::get('permissions','AdminController@permissions')->name("permissions");

Route::get('posts','AdminController@posts')->name("posts");

Route::get('roles','AdminController@roles')->name("roles");