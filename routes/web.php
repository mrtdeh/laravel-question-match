<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('about', 'AboutController@index')->name('about');


Route::get('users', 'UserController@index')->name('users');

Route::get('/user/{user}', 'ShowUserPosts')->name('userPosts');

Route::get('posts', 'PostController@index')->name('posts');

Route::get('posts/{post}', 'PostController@show')->name('showPost');

// Route::middleware("auth",function(){

    Route::get("match/start","MatchController@start");

// });

