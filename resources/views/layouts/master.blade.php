<html lang="en">
<head>
  <meta charset="utf-8">

  <title>The HTML5 Herald</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="{{ mix('css/app.css') }}">

  @yield("head")
 

</head>

<body>

<div id="app">

  @include('partials.navbar')
  
  @yield("body") 
  
  
</div>

  <script src="{{ mix('js/app.js') }}"></script>

  @yield("end")

</body>
</html>