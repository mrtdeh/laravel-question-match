@extends('layouts.master')

@section("body")
  
  <div class="container">
      <div class="row">
        @foreach($posts as $post)
          @include("partials.postBox",$post)
        @endforeach
      </div>
  </div>


@endsection