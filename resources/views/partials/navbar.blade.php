<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNav">
   
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>

    @if(Auth::user())
      <ul class="navbar-nav mr-3 position-absolute" style="right:0">
        <li class="nav-item">
          <a class="nav-link" href="/staff/dashboard">{{ Auth::user()->username }}</a>
        </li>
        <li class="nav-item">
          <a href="{{ route("logout") }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              Logout
          </a>
        </li>
      </ul>
      <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
    @else
      <ul class="navbar-nav mr-3 position-absolute" style="right:0">
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
      </ul>
    @endif
  </div>

</nav>