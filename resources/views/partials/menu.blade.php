<div class="sidebar">
    <nav class="sidebar-nav ps ps--active-y">

        <ul class="nav">
            <li class="nav-item">
                <a href="{{ route("admin.dashboard") }}" class="nav-link {{ request()->is('admin/dashboard') || request()->is('admin/dashboard/*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-car">

                    </i>
                    Dashboard
                </a>
            </li>
    
            <li class="nav-item">
                <a href="{{ route("admin.permissions") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                    <i class="fas fa-unlock-alt nav-icon">

                    </i>
                    Permitions
                </a>
            </li>
            
            <li class="nav-item">
                <a href="{{ route("admin.users") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                    <i class="fas fa-user nav-icon">

                    </i>
                    Users
                </a>
            </li>
           
            <li class="nav-item">
                <a href="{{ route("admin.posts") }}" class="nav-link {{ request()->is('admin/posts') || request()->is('admin/posts/*') ? 'active' : '' }}">
                    <i class="fas fa-book nav-icon">

                    </i>
                    Posts
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route("logout") }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-sign-out-alt">

                    </i>
                    Logout
                </a>
            </li>
        </ul>

        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 869px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
        </div>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>