<div class="col-sm-4">
    <div class="card " style="height:250px;margin:10px 0">
      <div class="card-body">
        <h5 class="card-title"><b>{{ $post->title }}</b></h5>
        <p class="card-text">{{ $post->content }}</p>
        <div class="buttons">
          <a href="#" class="btn btn-primary">Go somewhere</a>
        <span class="marging-top float-right"> by <a href="/{{ $post->user->username }}/posts">{{ $post->user->name }}</a></span>
        </div>
      </div>
    </div>
  </div>

<style>
    .buttons{
      position: absolute;
      bottom: 0;
      padding: 70px 10px 10px;
      background: linear-gradient(0, #7e949efc, transparent);
      left: 0;
      right: 0;
    }
    
</style>