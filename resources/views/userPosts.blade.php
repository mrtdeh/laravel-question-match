@extends('layouts.master')

@section("body")

  <div class="container">
      <div class="row mb-2">
          <div class="card p-2" style="width:100%;">
              <h3>{{ $selectedUser->name }} Posts :</h3>
          </div>
      </div>
      <div class="row">
        @foreach($selectedUser->posts as $post)
            @include("partials.postBox",$post)
        @endforeach
      </div>
  </div>




@endsection