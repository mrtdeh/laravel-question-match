@extends("layouts.admin")

@section("content")

<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Type</th>
        <th scope="col">Registered Date</th>
      </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{$loop->index+1}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                  @foreach($user->roles as $r) 
                    <span class="badge badge-info mr-1 p-1">{{ Ucfirst($r->name) }}</span> 
                  @endforeach
                </td>
                <td>{{$user->registered_date}}</td>
            </tr>
        @endforeach
    </tbody>
  </table>

@endsection