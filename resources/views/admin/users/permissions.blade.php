@extends("layouts.admin")

@section("content")


<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Permission</th>
        <th scope="col">Roles</th>
      </tr>
    </thead>
    <tbody>
        @foreach($permissions as $per)
            <tr>
                <th scope="row">{{$loop->index+1}}</th>
                <td>{{ Ucfirst($per->name) }} </td>
                <td>
                    @foreach($per->roles as $role)
                        <span class="badge-pill badge-info py-2 px-3 mr-2">
                        {{ Ucfirst($role->name) }}
                        </span>
                    @endforeach
                
                </td>
                
            </tr>
        @endforeach
    </tbody>
  </table>


@endsection