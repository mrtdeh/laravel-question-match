@extends("layouts.admin")

@section("content")


<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Created By</th>
        <th scope="col">Submited Date</th>
      </tr>
    </thead>
    <tbody>
        @foreach($posts as $post)
            <tr>
                <th scope="row">{{$loop->index+1}}</th>
                <td>{{$post->title}}</td>
                <td>{{$post->user->name}}</td>
                <td>{{$post->createdDate}}</td>
            </tr>
        @endforeach
    </tbody>
  </table>


@endsection